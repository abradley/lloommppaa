#### THIS REPOSITIORY IS DESIGNED FOR INTERNAL USE ONLY #####


# Install Docker #
https://www.docker.com/docker-toolbox

# Go into the Docker CLI

Type:

```
docker pull abradle/lloommppaa
```

Then pull down the git repo of code
```
git clone https://abradley@bitbucket.org/abradley/lloommppaa.git
cd lloommppaa
```
Make a directory that is called data
```
mkdir data
```

Now you can run a lloommppaa run -> you need the following options.

All files should be placed in data.
```
--mol2_prot -> The MOL2 file of the protein (with Hydrogens)
--products -> The SDF file of the products
--ll_prot -> The Protein with the bound ligand
--smiles -> The SMILES of the bound ligand
--context -> The part of the molecule to hold static
--name -> The name of htis run -> must be unique
```

NOTE HOW THE PATHS ARE ALL /data/ -> THIS IS IMPORTANT
```
bash do_ll.bash --name MEx337 --mol2_prot /data/X200_SETDB1_TEMP.mol2 --products /data/N13441a_methylesters.sdf --ll_prot /data/N13441a-x337.pdb --lloommppaa True --smiles "COC(=O)c1cccc(c1)NS(=O)(=O)C" --context "COC(=O)c1cccc(NS(C)(=O)=O)c1"
```
